package fr.cesi.forge.message;

import java.util.ResourceBundle;

public class GestionMessage {
	
	private static ResourceBundle labels = ResourceBundle.getBundle("i18n");
/**
 * Lit un message dans un fichier.
 * 
 * @param key cle dont on lit la valeur
 * 
 * @return le message associe a la cle
 */
	public static String getMessage( String key) {
		
		String retour = labels.getString(key);
		
		return retour;
		
	}

}
