package helloModule.Hello2.src.main.java.fr.cesi.forge;


import org.slf4j.Logger; 
import org.slf4j.LoggerFactory; 

import fr.cesi.forge.message.GestionMessage;
import helloModule.Hello2.src.main.java.fr.cesi.forge.GereMessage;;

/**
 * Ceci est la classe principale.
 * 
 * @author 2129158
 *
 */
public class Hello { 

	private static final Logger logger = LoggerFactory.getLogger(Hello.class); 
/**
 * Point d'entree.
 * 
 * R�alise pendant le cours sur l'integration continue.
 * 
 * 
 * @param args
 */
	public static void main( String [] args ){

		GereMessage message = new GereMessage();
		String bonjour = message.gereMessage("msg_hello");
		

		logger.debug("Avant le message"); 

		System.out.println(bonjour) ; 

		logger.debug("Apres le message"); 

	} 

} 